@tool
extends EditorPlugin



# ----------
# Properties
# ----------

var plugin_name: String = "In-game Console"
var plugin_root: String = "res://addons/ingame_console"

var singletons: Array[Dictionary] = [
	{
		"name": "Console",
		"script_path": "res://addons/ingame_console/scripts/console.gd"
	},
]



# ---------------
# Virtual methods
# ---------------

func _enter_tree() -> void:
	message("Loading...")
	load_singletons()
	message("Loaded")


func _exit_tree() -> void:
	message("Unloading...")
	unload_singletons()
	message("Unloaded")



# --------------------------
# Loading, unloading methods
# --------------------------

func load_singletons() -> void:
	for singleton in singletons:
		add_autoload_singleton(
			singleton.get("name"),
			singleton.get("script_path")
		)


func unload_singletons() -> void:
	for singleton in singletons:
		remove_autoload_singleton(singleton.get("name"))


func message(text: String) -> void:
	print("%s: %s" % [plugin_name, text])
