class_name Command extends Object



## For storing data about console commands



# ----------
# Properties
# ----------

## The ID of the Command, this is used for calling the command from to console
var id: String
## The callback of the Command, this is called when the command is ran
var callback: Callable
## The arguments that the Command should parse, validate, convert, and pass to the callback
var arguments: Array[Arg] = []
## This is displayed when the command is printed with the [code]commands[/code] console command
var short_description: String
## This is displayed when the command information is printed with the [code]help <command>[/code] console command
var long_description: String



# ---------------
# Virtual methods
# ---------------

## Constructor
func _init(
	id: String,
	callback: Callable = func(): pass,
	arguments: Array[Arg] = [],
	short_description: String = "",
	long_description: String = ""
) -> void:
	self.id = id
	self.callback = callback
	self.arguments.append_array(arguments)
	self.short_description = short_description
	self.long_description = long_description



# -------
# Methods
# -------

## Processes the command, parses the arguments from the passed command string,
## validates them, and passes them to the callback.
## Returns a Dictionary that contains the error messages, if there were no
## errors then an empty dictionary is returned.
func run(command_string: String) -> Dictionary:
	var parsed_args: Dictionary = parse_string_args(command_string)
	var result: Dictionary = validate(parsed_args)

	if not result["messages"].is_empty():
		return result["messages"]

	callback.call(result["args"])

	return {}


## Parses string for arguments, and returns them in an array.
## Input:
func parse_string_args(text: String) -> Dictionary:
	var prefix_length: int = Console.command_parameter_prefix.length()
	text = text.lstrip(id) + " "

	var args: Dictionary = {
		"0": id.split("|")[0]
	}
	var current_key: String = ""
	var current_value_index: int = 1

	var i: int = 0
	while i < text.length():
		if text.substr(i, prefix_length) == Console.command_parameter_prefix:
			if current_key:
				args[current_key] = null
				current_key = ""

			var key_end: int = text.find(" ", i)
			current_key = text.substr(i + 2, key_end - i - 2)
			i = key_end

		elif text[i] == "\"" or text[i] != " ":
			var offset: int = 1 if text[i] == "\"" else 0
			var value_end_index: int = text.find("\"" if text[i] == "\"" else " ", i + 1)
			var value: String = text.substr(i + offset, value_end_index - i - offset)

			if current_key:
				var argument: Arg = get_argument(current_key)
				args[argument.get_main_id() if argument else current_key] = value
				current_key = ""
			else:
				var argument: Arg = get_argument(str(current_value_index))
				args[argument.get_main_id() if argument else str(current_value_index)] = value
				current_value_index += 1
			i = value_end_index

		i += 1

	if current_key:
		args[current_key] = null

	return args


func validate(passed_args: Dictionary) -> Dictionary:
	var result: Dictionary = {
		"args": passed_args, "messages": {}
	}
	for passed_arg_key in passed_args:
		var argument: Arg = get_argument(passed_arg_key)
		if not argument:
			continue

		var validator_messages: Array[String] = argument.validate(passed_args[passed_arg_key])
		if not validator_messages.is_empty():
			result["messages"][passed_arg_key] = validator_messages
		else:
			result["args"][passed_arg_key] = argument.get_value(passed_args[passed_arg_key])

	return result


func set_callback(callback: Callable) -> Command:
	self.callback = callback

	return self


func add_argument(argument: Arg) -> Command:
	arguments.push_back(argument)

	return self


func get_argument(id: String) -> Arg:
	for argument in arguments:
		if id in argument.ids:
			return argument

	return null


func set_short_description(description: String) -> Command:
	self.short_description = description

	return self


func set_long_description(description: String) -> Command:
	self.long_description = description

	return self
