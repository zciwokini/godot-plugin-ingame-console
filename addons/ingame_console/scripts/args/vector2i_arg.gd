class_name Vector2iArg extends VariantArg



## Syntax: Vector2i(1,2), (1,2), 1,2
func get_value(value: Variant) -> Variant:
	begins_withs = ["Vector2i", "("]
	ends_withs = [")"]
	
	value = _parse_value(value)
	
	return value if value is Vector2i else null
