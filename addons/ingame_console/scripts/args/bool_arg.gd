class_name BoolArg extends Arg



func get_value(value: Variant) -> Variant:
	return str(value).to_lower() == "true"
