class_name FloatArg extends Arg



func get_value(value: Variant) -> Variant:
	return str(value).to_float() if str(value).is_valid_float() else null
