class_name VariantArg extends Arg



var begins_withs: Array[String] = []
var ends_withs: Array[String] = []



func get_value(value: Variant) -> Variant:
	value = _parse_value(value)
	
	return value


func _parse_value(value: Variant) -> Variant:
	value = str(value).strip_edges()
	
	for i in range(begins_withs.size() - 1, -1, -1):
		if not value.begins_with(begins_withs[i]):
			value = begins_withs[i] + value
	
	for end_with in ends_withs:
		if not value.begins_with(end_with):
			value = value + end_with
	
	return str_to_var(value)
