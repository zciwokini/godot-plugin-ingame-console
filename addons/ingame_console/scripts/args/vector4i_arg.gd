class_name Vector4iArg extends VariantArg



## Syntax: Vector4i(1,2,3), (1,2,3), 1,2,3
func get_value(value: Variant) -> Variant:
	begins_withs = ["Vector4i", "("]
	ends_withs = [")"]
	
	value = _parse_value(value)
	
	return value if value is Vector4i else null
