class_name IntArg extends Arg



func get_value(value: Variant) -> Variant:
	return str(value).to_int() if str(value).is_valid_int() else null
