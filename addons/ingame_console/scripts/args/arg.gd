class_name Arg extends Object



# ----------
# Properties
# ----------

var ids: Array[String] = []
var validators: Array[IValidator] = []
var description: String = ""



# ---------------
# Virtual methods
# ---------------

func _init(ids: String, validators: Array[IValidator] = [], description: String = "") -> void:
	for id in Array(ids.split(",", false)):
		self.ids.push_back(id.strip_edges())
	self.validators.append_array(validators)
	self.description = description



# -------
# Methods
# -------

func add_validator(validator: IValidator) -> Arg:
	validators.push_back(validator)

	return self


func set_description(description: String) -> Arg:
	self.description = description

	return self


func validate(value: Variant) -> Array[String]:
	var messages: Array[String] = []
	for validator in validators:
		if not validator.validate(value):
			messages.push_back(validator.message.format({"value": value}))

	return messages


func get_value(value: Variant) -> Variant:
	return value


func get_main_id() -> String:
	if ids.size():
		return ids[0]
	return ""
