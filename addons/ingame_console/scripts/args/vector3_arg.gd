class_name Vector3Arg extends VariantArg



## Syntax: Vector3(1,2,3), (1,2,3), 1,2,3
func get_value(value: Variant) -> Variant:
	begins_withs = ["Vector3", "("]
	ends_withs = [")"]
	
	value = _parse_value(value)
	
	return value if value is Vector3 else null
