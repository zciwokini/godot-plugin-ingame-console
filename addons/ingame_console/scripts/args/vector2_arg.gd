class_name Vector2Arg extends VariantArg



## Syntax: Vector2(1,2), (1,2), 1,2
func get_value(value: Variant) -> Variant:
	begins_withs = ["Vector2", "("]
	ends_withs = [")"]
	
	value = _parse_value(value)
	
	return value if value is Vector2 else null
