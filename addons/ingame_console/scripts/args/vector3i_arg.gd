class_name Vector3iArg extends VariantArg



## Syntax: Vector3i(1,2,3), (1,2,3), 1,2,3
func get_value(value: Variant) -> Variant:
	begins_withs = ["Vector3i", "("]
	ends_withs = [")"]
	
	value = _parse_value(value)
	
	return value if value is Vector3i else null
