class_name Vector4Arg extends VariantArg



## Syntax: Vector4(1,2,3,4), (1,2,3,4), 1,2,3,4
func get_value(value: Variant) -> Variant:
	begins_withs = ["Vector4", "("]
	ends_withs = [")"]
	
	value = _parse_value(value)
	
	return value if value is Vector4 else null
