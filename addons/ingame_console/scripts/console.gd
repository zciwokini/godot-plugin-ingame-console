extends Node

## Console Global Singleton
##
## Singleton for using the Console from anywhere in your projects, this singleton handles, the formatting,

# ---------
# Constants
# ---------


enum LogLevel { INFO = 1, DEBUG = 2, WARNING = 3, ERROR = 4 }


# ----------
# Properties
# ----------


## Whether the messages written to te console should be printed to the standard output
var log_level: LogLevel = LogLevel.ERROR
var include_default_commands: bool = true
var halt_on_error: bool = false
var is_logging: bool = true
var log_format: String = "{date} {time} [b][color={category_color}]{category_name}[/color][/b] {text}"
var _log_categories: Dictionary = {}

var similarity_threshold: float = 0.5

var _commands: Dictionary = {}
var _command_ids: Array[String] = []
var command_parameter_prefix: String = "--"

var _bbcode_parser: RichTextLabel


# -------
# Signals
# -------


## Emitted when a message is printed via the Console
signal message(text: String)
## Same as the message signal, but the message contains the BBCode
signal message_bbcode(text: String)
## Request to clear the console content
signal clear()


# ---------------
# Virtual methods
# ---------------


func _ready() -> void:
	_generate_dummy_parser()
	_add_defaults()


# ----------------
# Category methods
# ----------------


func add_category(id: String, name: String = "", color: String = "") -> void:
	_log_categories[id] = {
		"category_name": name,
		"category_color": color
	}


func remove_category(id: String) -> void:
	_log_categories.erase(id)


# -----------------
#  Command methods
# -----------------


func add_command(command: Command) -> void:
	_commands[command.id] = command
	_command_ids.push_back(command.id)


func remove_command(id: String) -> void:
	_commands.erase(id)
	_command_ids.remove_at(_command_ids.find(id))


func process_command(text: String) -> void:
	var command_root: String = text.split(" ", false, 1)[0]
	if not command_root in _commands:
		writeline("Unknown command: %s" % command_root)
		print_similar_commands(command_root)
		newline()
		return

	var command: Command = _commands[command_root]
	var args: Dictionary = {}

	var messages: Dictionary = command.run(text)
	for arg in messages:
		writeline("Validation error(s) for [u]%s[/u] argument:" % arg)
		for message in messages[arg]:
			writeline("[indent]%s[/indent]" % message)
		newline()


func print_similar_commands(command: String) -> void:
	var similar_commands: Array[Dictionary] = []
	for command_id in _command_ids:
		if command.similarity(command_id) > similarity_threshold:
			similar_commands.push_back({
				"name": command_id,
				"similarity": command.similarity(command_id),
			})

	similar_commands.sort_custom(func(a, b): return a["similarity"] < b["similarity"])

	if not similar_commands.is_empty():
		writeline("Similar commands: %s" % ", ".join(similar_commands.map(func(c): return c["name"])))


# ----------------
# Printing methods
# ----------------


func log(category_id: String, text, level: LogLevel = LogLevel.INFO) -> void:
	if level > log_level:
		return

	var format_values: Dictionary = {
		"text": text,
		"date": Time.get_date_string_from_system(),
		"time": Time.get_time_string_from_system()
	}

	if category_id in _log_categories:
		format_values.merge(_log_categories[category_id])

	writeline("%s" % log_format.format(format_values), true, level)


func info(text: Variant) -> void:
	self.log("info", text)


func debug(text: Variant) -> void:
	self.log("debug", text)


func warning(text: Variant) -> void:
	self.log("warning", text, LogLevel.WARNING)


func error(text: Variant) -> void:
	self.log("error", text, LogLevel.ERROR)


func write(text: Variant = "", is_logging: bool = false, level: LogLevel = LogLevel.INFO) -> void:
	text = str(text)

	_bbcode_parser.parse_bbcode(text)
	var parsed_text: String = _bbcode_parser.get_parsed_text()

	message.emit(parsed_text)
	message_bbcode.emit(text)

	if text.ends_with("\n"):
		text = "".join(text.rsplit("\n", true, 1))
		parsed_text = "".join(parsed_text.rsplit("\n", true, 1))

	if is_logging and level <= log_level:
		if level == LogLevel.WARNING:
			push_warning(parsed_text)
		elif level == LogLevel.ERROR:
			if Console.halt_on_error:
				assert(false, "Halting because of an error")
			else:
				push_error(parsed_text)
		else:
			if OS.has_feature("editor"):
				print_rich(text)
			else:
				print(parsed_text)


func writeline(text: Variant = "", is_logging: bool = false, level: LogLevel = LogLevel.INFO) -> void:
	write(str(text) + "\n", is_logging, level)


func newline(is_logging: bool = false, level: LogLevel = LogLevel.INFO) -> void:
	write("\n", is_logging, level)


func _clear(args: Dictionary) -> void:
	clear.emit()


# -----------------------
# Console command methods
# -----------------------


func _help(args: Dictionary) -> void:
	for arg in args:
		if arg in _command_ids:
			_list_command_info(arg)
			return

	if "command" in args:
		_list_command_info(args["command"])
		return

	_list_command_info("help")


func _list_commands(args: Dictionary) -> void:
	var content: String = ""
	_command_ids.sort()
	for command_id in _command_ids:
		var command: Command = _commands[command_id]
		content += "[cell]%s [/cell] [cell]%s\n[/cell]" % [command.id, command.short_description]
	writeline("[indent][table=2]\n%s[/table][/indent]\n" % content)


func _list_command_info(command_id: String) -> void:
	if not command_id in _commands:
		return

	var command: Command = _commands[command_id]
	writeline("Information about the following command: [u]%s[/u]\n" % command_id)
	writeline("%s\n" % command.short_description)

	if command.long_description:
		writeline("Description:")
		writeline("[indent]%s[/indent]\n" % command.long_description)

	if not command.arguments.is_empty():
		writeline("Argument(s):")

		var content: String = ""
		for argument in command.arguments:
			content += "[cell]%s: [/cell] [cell]%s\n[/cell]" % [
				", ".join(argument.ids.map(
					func(id: String): return id if id.is_valid_int() else "--" + id)
				),
				argument.description
			]

		writeline("[indent][table=2]%s[/table][/indent]\n" % content)


# --------------
# Helper methods
# --------------


func _generate_dummy_parser() -> void:
	_bbcode_parser = RichTextLabel.new()
	_bbcode_parser.name = "DummyBBCodeParser"
	_bbcode_parser.visible = false

	# Adding it to the scene, so it does not appear as an orphan node
	add_child(_bbcode_parser)


func _add_defaults() -> void:
	var default_categories: Array[Dictionary] = [
		{ "id": "info", "name": "INFO" },
		{ "id": "debug", "name": "DEBUG", "color": "lightblue" },
		{ "id": "warning", "name": "WARNING", "color": "orange" },
		{ "id": "error", "name": "ERROR", "color": "red" },
	]

	for category in default_categories:
		add_category(
			category.get("id"),
			category.get("name"),
			category.get("color", "")
		)

	var default_commands: Array[Command] = [
		Command.new("commands", _list_commands, [], "Lists the available commands."),
		Command.new("clear")\
			.set_callback(_clear)\
			.set_short_description("Clears the content of the console."),

		Command.new("quit")\
			.set_callback(get_tree().quit)\
			.set_short_description("Terminates the application"),

		Command.new(
			# Command's name(ID)
			"help",
			# Command's function(callback)
			_help,
			# Command's arguments
			[
				Arg.new(
					# Argument's name (IDs)
					"command, c, 1",
					# Argument's validators
					[
						StringsValidator.new(_command_ids, "[u]{value}[/u] is not an existing command.")
					],
					# Argument's short description
					"The ID of the command of which it should print information about."
				)
			],
			# Command's short description
			"Lists information about a given command and it's usage.",
			# Command's long description
			"""Type a command ID after the help command to get info about the given command.
For example, type: [u]help commands[/u] to list information about the [u]commands[/u] command."""
		),
	]

	if include_default_commands:
		for default_command in default_commands:
			add_command(default_command)
