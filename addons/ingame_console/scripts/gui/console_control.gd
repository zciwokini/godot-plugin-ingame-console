@icon("res://addons/ingame_console/images/console_icon.svg")
@tool
class_name ConsoleControl extends Control


## asd


# ----------
# Properties
# ----------


@export var generate_gui_on_ready: bool = true
@export var is_bbcode_enabled: bool = true:
	set(value):
		if Console.message.is_connected(write):
			Console.message.disconnect(write)
		if Console.message_bbcode.is_connected(write):
			Console.message_bbcode.disconnect(write)

		is_bbcode_enabled = value

		if is_bbcode_enabled:
			Console.message_bbcode.connect(write)
		else:
			Console.message.connect(write)

@export var input_prefix: String = " > "

@export var console_input: LineEdit:
	set(value):
		console_input = value

		console_input.text_submitted.connect(_on_text_entered)

@export var console_content: RichTextLabel:
	set(value):
		console_content = value
		scrollbar = console_content.get_v_scroll_bar()

		if is_bbcode_enabled:
			if not Console.message_bbcode.is_connected(write):
				Console.message_bbcode.connect(write)
		else:
			if not Console.message.is_connected(write):
				Console.message.connect(write)
		if not Console.clear.is_connected(clear):
			Console.clear.connect(clear)

var scrollbar: VScrollBar


# ---------------
# Virtual methods
# ---------------


func _ready() -> void:
	if generate_gui_on_ready:
		_generate_gui()


# -------
# Methods
# -------


## Writes to the console without newline.
func write(text: Variant) -> void:
	console_content.append_text(str(text))

	if scrollbar:
		if scrollbar.value >= scrollbar.max_value - scrollbar.page:
			console_content.scroll_to_line(console_content.get_line_count())


func writeline(text: Variant) -> void:
	write(str(text) + "\n")


## Cleares the content of the console
func clear() -> void:
	console_content.clear()


func _generate_gui() -> void:
	var default_console: PackedScene = load("res://addons/ingame_console/scenes/default_console.tscn")

	add_child(default_console.instantiate())

	self.console_content = find_child("Content", true, false)
	self.console_input = find_child("Input", true, false)


# -------------
# Event methods
# -------------


func _on_text_entered(text: String) -> void:
	if text.is_empty():
		return

	console_input.clear()
	write("%s%s\n" % [input_prefix, text])
	Console.process_command(text)
