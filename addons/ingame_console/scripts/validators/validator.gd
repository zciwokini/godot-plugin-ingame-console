class_name IValidator extends Object



# ----------
# Properties
# ----------

var validation: Variant
var message: Variant



# ---------------
# Virtual methods
# ---------------

func _init(validation: Variant, message: Variant) -> void:
	self.validation = validation
	self.message = message


func validate(value: Variant) -> bool:
	return true
