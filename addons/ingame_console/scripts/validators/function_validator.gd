class_name FunctionValidator extends IValidator



func validate(value: Variant) -> bool:
	return self.validation.call_funcv([value])
