class_name RegexValidator extends IValidator


func validate(value: Variant) -> bool:
	var regex: RegEx = RegEx.new()
	regex.compile(self.validation)

	return regex.search(value) != null
