class_name StringsValidator extends IValidator



func validate(value: Variant) -> bool:
	for v in self.validation:
		if value.match(v):
			return true

	return false
