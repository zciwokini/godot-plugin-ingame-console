<div align="center">
	<img src="docs/images/icon.png" width="100px">
	<h1><b>In-Game Console Plugin<br>for <a href="https://godotengine.org">Godot Engine</b></a></h1>
</div>


A simple and easy to use console for your applications. You can see messages without the editor easily and use commands that help you test features.


<div align="center">
	<img src="docs/images/screenshot_1.png" width="700px">
</div>


Installation
------------

You can install it from the [Godot Asset Store][GodotAssetStore].

Or manually download the repository, copy the `addons/ingame_console` folder into the `addons/` folder in your Godot project, and lastly enable the plugin in the Project Settings.


Documentation
-------------

If you are new, please read the [Get Started][GetStarted] documentation page or check all the [Documentations][Documentations].

All the public properties and methods are documented and are available in the Godot Engine Editor.


Features
--------

### **Logging** (*info, warning, error, or any custom category*)

```swift
Console.info("Player respawning in 3 seconds")

Console.warning("Player is too far from the map")

Console.error("This shouldn't have happened")

Console.add_category("network", "Network", "blue")
Console.log("network", "Player connected to the server")
```

### **Commands** (*arguments, validators*)

```swift
func _ready() -> void:
	var random_command: Command = Command.new("random")\
		.set_callback(print_random_number)\
		.add_argument(IntArg.new("min, 1"))\
		.add_argument(IntArg.new("max, 2"))

	Console.add_command(random_command)


func print_random_number(args: Dictionary) -> void:
	var min: int = args["min"]
	var max: int = args["max"]

	var randnum: int = randi() % (max - min) + min
	Console.writeline("Random number: %s" % randnum)
```

To use this command, type in one of the following commands in the console:

```
random 10 30
```

```
random --min 10 --max 30
```


Supported versions
------------------

Currently this plugin only works with the 4.x Godot Engine versions.


License
-------

This plugin is licensed under the [MIT License][License].


[License]: LICENSE.md
[Documentations]: docs/documentations.md "Documentations - In-game Console Documentation"
[GetStarted]: docs/get_started.md "Get Started - In-game Console Documentation"
[GodotAssetStore]: https://godotengine.org/asset-library/asset
