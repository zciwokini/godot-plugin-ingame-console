extends Node


func _ready() -> void:
	method_chaining()


func method_chaining() -> void:
	var random_command: Command = Command.new("random")\
		.set_callback(print_random_number)\
		.add_argument(
			IntArg.new("min, 1")\
				.add_validator(RegexValidator.new("\\d", "Not a number"))
		)\
		.add_argument(
			IntArg.new("max, 2")\
				.add_validator(RegexValidator.new("\\d", "Not a number"))
		)

	Console.add_command(random_command)


func constructor_only() -> void:
	var random_command: Command = Command.new(
		"random",
		print_random_number,
		[
			IntArg.new("min, 1", [
				RegexValidator.new("\\d", "Not a number")
			]),
			IntArg.new("max, 2", [
				RegexValidator.new("\\d", "Not a number")
			]),
		]
	)

	Console.add_command(random_command)


func print_random_number(args: Dictionary) -> void:
	var min_value: int = args["min"]
	var max_value: int = args["max"]

	var random_number: int = randi() % (max_value - min_value) + min_value
	Console.writeline("Random number: %s" % random_number)
